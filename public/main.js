const LOCAL = document.getElementById('local');
const REMOTE = document.getElementById('remote');
const STOP_BUTTON = document.getElementById('stopBtn');
const USERS_ONLINE_DIV = document.getElementById('users_online');
const GLOBAL_MESSAGE = document.getElementById('global-message');
const CHAT = document.getElementById('chat');
const SEND_CHAT = document.getElementById('send-chat-btn');
const CAM_BTN = document.getElementById('camBtn');
const CALLING_TXT = document.getElementById('callingTxt');
const CALLING_BOX = document.getElementById('callingBox');
const ANSWER_BTN = document.getElementById('answerBtn');
const REJECT_BTN = document.getElementById('rejectBtn');

const WS = io('http://localhost:3000/');
const CONFIGURATION = {
  iceServers: [{ url: 'stun:stun2.1.google.com:19302' }],
};

let PC = null;
let USER_SOCKET_ID = null;
let USER_TARGET_ID = null;
let LOCAL_STREAM = null;
let USERS_ONLINE = [];
let IS_OPENCAM = false;
let CALL_OFFER = {
  to: '',
  from: '',
  offer: null,
};

ANSWER_BTN.addEventListener('click', () => {
  answerCall();
  CALLING_BOX.style.display = 'none';
});

REJECT_BTN.addEventListener('click', () => {
  rejectCall();
  CALLING_BOX.style.display = 'none';
});

CAM_BTN.addEventListener('click', () => {
  if (IS_OPENCAM) {
    closeCam();
  } else {
    openCam();
  }
});

openCam();

GLOBAL_MESSAGE.addEventListener('keyup', (event) => {
  if (event.key === 'Enter') {
    WS.emit('global-message', GLOBAL_MESSAGE.value);
    GLOBAL_MESSAGE.value = '';
  }
});

STOP_BUTTON.addEventListener('click', () => {
  console.log('clicked', USER_TARGET_ID);
  if (USER_TARGET_ID != null) {
    WS.emit('close', { to: USER_TARGET_ID });
  }

  USER_TARGET_ID = null;
  REMOTE.src = null;

  PC.close();
  PC.onicecandidate = null;
  PC.onaddstream = null;
  openPC();
});

async function answerCall() {
  const { offer, to, from } = CALL_OFFER;
  await PC.setRemoteDescription(new RTCSessionDescription(offer));
  const answer = await PC.createAnswer();
  PC.setLocalDescription(answer);

  WS.emit('answer', {
    from: to,
    to: from,
    answer,
  });
}

function rejectCall() {
  const { to, from } = CALL_OFFER;

  WS.emit('answer', {
    from: to,
    to: from,
    answer: 'rejected',
  });
}

async function openCam() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true,
    });
    LOCAL_STREAM = stream;
    LOCAL.srcObject = stream;

    IS_OPENCAM = true;
    CAM_BTN.innerHTML = 'Close Cam';
    openPC();
  } catch (e) {
    console.error(e);
    if (e.message === 'Permission denied') {
      alert(
        'We need permission to access your camera, so please set your permision to allow us access your camera'
      );
    }
  }
}

function closeCam() {
  LOCAL_STREAM.getTracks().forEach((track) => {
    track.stop();
  });
  IS_OPENCAM = false;
  CAM_BTN.innerHTML = 'Open Cam';
}

function showUsers(users) {
  USERS_ONLINE_DIV.innerHTML = '';

  users.forEach((i) => {
    USERS_ONLINE_DIV.innerHTML += `<div class="button callBtn" id="${i.id}" ${
      i.oncall ? 'disabled' : ''
    }>Call ${i.id}</div>`;
  });
}

document.addEventListener('click', call);

function openPC() {
  console.log('openPc invoked');
  PC = new RTCPeerConnection(CONFIGURATION);
  for (const track of LOCAL_STREAM.getTracks()) {
    PC.addTrack(track, LOCAL_STREAM);
  }

  PC.ontrack = (event) => {
    if (event.streams && event.streams[0]) {
      console.log('ontrack', event.streams);
      // this.remoteStream.push(event.streams[0])
      REMOTE.srcObject = event.streams[0];
    }
  };

  PC.onicecandidate = (event) => {
    console.log('onicecandidate', event);

    if (event.candidate && USER_TARGET_ID != null) {
      const data = {
        candidate: event.candidate,
        from: USER_SOCKET_ID,
        to: USER_TARGET_ID,
      };
      WS.emit('candidate', data);
    }
  };

  PC.onnegotiationneeded = async () => {
    try {
      const offer = await PC.createOffer();
      await PC.setLocalDescription(offer);
      // Send the offer to the remote peer through the signaling server
      if (USER_TARGET_ID != null) {
        const data = {
          from: USER_SOCKET_ID,
          to: USER_TARGET_ID,
          offer,
        };
        WS.emit('offer', data);
      }
    } catch (e) {
      console.error('err onnegotiationneeded', e);
    }
  };
}

async function call(event) {
  const element = event.target;
  if (element.classList.contains('callBtn')) {
    const id = event.target.id;
    USER_TARGET_ID = id;
    openPC();
  }
}

WS.on('connect', () => {
  console.log('connected', WS.id);
  USER_SOCKET_ID = WS.id;
});

WS.on('candidate', ({ from, to, candidate }) => {
  try {
    if (IS_OPENCAM && candidate) {
      console.log('handleCandidate', candidate);
      const can = new RTCIceCandidate(candidate);
      // console.log({ can })
      PC.addIceCandidate(can).catch((e) => {
        console.log('Failure during addIceCandidate(): ' + to + ' ' + e);
      });
    }
  } catch (e) {
    console.log('err', e);
  }
});

WS.on('offer', async ({ from, to, offer }) => {
  try {
    console.log('handleOffer', from, offer);
    USER_TARGET_ID = from;
    CALL_OFFER = {
      to,
      from,
      offer,
    };

    CALLING_TXT.innerHTML = `${from} is calling, would you like to answer?`;
    CALLING_BOX.style.display = 'inline';
  } catch (e) {
    alert('Error when creating an answer');
    console.error(e);
  }
});

WS.on('answer', ({ from, to, answer }) => {
  console.log('handleAnswer', to, answer);

  if (answer === 'rejected') {
    alert(`${from} rejected the call`);
  } else {
    PC.setRemoteDescription(new RTCSessionDescription(answer)).catch((e) => {
      console.log('err answer setRemoteDescription', e);
      console.log('err answer setRemoteDescription userId', to);
    });
  }
});

WS.on('users', (users) => {
  USERS_ONLINE = users
    .filter((i) => i.id !== USER_SOCKET_ID)
    .filter((i) => !i.oncall);
  console.log('on users', users);

  showUsers(USERS_ONLINE);
});

WS.on('global-message', (data) => {
  console.log('global-MessageChannel', data);
  CHAT.innerHTML += `<div>${data.from}: ${data.data}</div>`;
});

WS.on('close', () => {
  console.log('on close');

  USER_TARGET_ID = null;
  REMOTE.src = null;

  PC.close();
  PC.onicecandidate = null;
  PC.onaddstream = null;
  openPC();
});
