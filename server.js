const express = require('express');
const app = express();
const http = require('http').createServer(app);

const io = require('socket.io')(http);

app.use(express.static('public'));
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

let users = {};

function changeOncall(arr, bool) {
  arr.forEach((i) => {
    users[i] = { id: i, oncall: bool };
  });
}

function toArr(obj) {
  return Object.entries(obj).map(([key, _]) => {
    return users[key];
  });
}

io.on('connection', (socket) => {
  console.log('someone connected', socket.id);
  const clients = io.sockets.clients().connected;
  users[socket.id] = { id: socket.id, oncall: false };
  const usersConn = toArr(clients);

  io.emit('users', usersConn);

  socket.on('connect', () => {
    console.log('connect', socket);
  });

  socket.on('global-message', (data) => {
    console.log('global-message', data);
    io.emit('global-message', { from: socket.id, data });
  });

  socket.on('oncall', (data) => {
    changeOncall([data.id1, data.id2], data.oncall);
    io.emit('users', toArr(clients));
  });

  socket.on('candidate', (data) => {
    console.log(`Sending candidate from ${data.from} to ${data.to}`);
    socket.to(data.to).emit('candidate', {
      from: socket.id,
      to: data.to,
      candidate: data.candidate,
    });
  });

  socket.on('offer', (data) => {
    console.log(`Sending offer from ${socket.id} to ${data.to}`);
    socket.to(data.to).emit('offer', {
      to: data.to,
      from: socket.id,
      offer: data.offer,
    });
  });

  socket.on('answer', (data) => {
    console.log(`Sending answer from ${socket.id} to ${data.to}`);
    socket.to(data.to).emit('answer', {
      to: data.to,
      from: socket.id,
      answer: data.answer,
    });

    if (data.answer !== 'rejected') {
      changeOncall([socket.id, data.to], true);
    }

    io.emit('users', toArr(clients));
  });

  socket.on('close', (data) => {
    console.log(`closed from ${socket.id} to ${data.to}`);

    changeOncall([socket.id, data.to], false);

    io.emit('users', toArr(clients));
    socket.to(data.to).emit('close');
  });

  socket.on('disconnect', () => {
    console.log('disconnected', socket.id);
    io.emit('users', toArr(clients));
    io.emit(
      'users',
      toArr(clients).filter((i) => i.id !== socket.id)
    );
  });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});
